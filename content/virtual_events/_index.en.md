---
title: Virtual Events
disableToc: true
weight: 39
chapter: true
---

#### {{< open-in-blank "Mert to the Future #08 DevOps Adoption Strategies: Principles, Processes, Tools, and Trends with Martyn Coupland - Thursday 19th August - 4.00pm - 5:00pm BST" "https://twitch.tv/azureishlive" >}}
![Azureish Live!](/images/events/mert-to-the-future-08.png?width=50pc)

---

#### {{< open-in-blank "Mert to the Future #07: Are You Secured? Are You Sure? with Tim Hermine & Wim Matthyssen - Thursday 12th August - 7.00pm - 8:00pm BST" "https://twitch.tv/azureishlive" >}}
![Azureish Live!](/images/events/mert-to-the-future-07.png?width=50pc)

---

#### {{< open-in-blank "Mert to the Future #06: Serverless in Azure with Vaibhav Gujral- Thursday 1st July - 5.00pm - 6:00pm BST" "https://youtu.be/E3z-HQAhsOc" >}}
![Azureish Live!](/images/events/mert-to-the-future-06.png?width=50pc)

---

#### {{< open-in-blank "Mert to the Future #05: What it takes to be an MVP? with Deepak Rajendran - Thursday 24th June - 5.00pm - 6:00pm BST" "https://youtu.be/kjEwOk1GyUM" >}}
![Azureish Live!](/images/events/mert-to-the-future-05.png?width=50pc)

---

#### {{< open-in-blank "Mert to the Future #04: Azure Durable Functions with Jonah Andersson - Thursday 17th June - 7.00pm - 8:00pm BST" "https://youtu.be/w13KS3b2ZDs" >}}
![msHOWTO Live!](/images/events/mert-to-the-future-04.png?width=50pc)

---

[Archived Virtual Events](/virtual_events/archived_virtual_events)

---